import 'package:flutter/material.dart';
import 'dart:ui';

void main() {
  runApp(new MaterialApp(
    home: new MyHomePage(),
  ));
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF353362),
      body: Container(
          decoration: BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("assets/boxping.png"),
                fit: BoxFit.fitWidth,
                alignment: Alignment.topLeft),
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomCenter,
              stops: [0.3, 0.9],
              colors: [
                Color(0xFF353569),
                Color(0xFF25263B),
              ],
            ),
          ),
          child: Center(
              child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(
                    left: 24.0, right: 24.0, top: 70.0, bottom: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Flexible(
                      child: Text(
                        'Classify Transaction',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 24.0,
                          //fontFamily: 'GoogleSans',
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                  padding: const EdgeInsets.only(
                      left: 24.0, right: 100.0, top: 0.0, bottom: 8.0),
                  child: Row(
                    children: <Widget>[
                      Flexible(
                        child: Text(
                          'Classify this transaction into a particular category',
                          style: TextStyle(
                            color: Colors.white54,
                            fontSize: 17.0,
                            //fontFamily: 'GoogleSans',
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ],
                  )),
              Expanded(
                child: ListView(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 28.0, right: 28.0, top: 24.0, bottom: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          ClipRRect(
                            borderRadius:
                                        BorderRadius.all(Radius.circular(30.0)),
                            child: BackdropFilter(
                              filter: new ImageFilter.blur(
                                  sigmaX: 20.0, sigmaY: 20.0),
                              child: new Container(
                                width: MediaQuery.of(context).size.width * 0.415,
                                height: 180.0,
                                decoration: new BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(30.0)),
                                    color: Color(0xFF4b4c6d).withOpacity(0.5)),
                                child: new Center(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 36.0),
                                    child: Column(
                                      children: <Widget>[
                                        CircleButton(
                                          onTap: () => print(""),
                                          iconData: Icons.view_module,
                                          colorR: Color(0xFF32A2FA),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 14.0),
                                        ),
                                        Text(
                                          'General',
                                          style: TextStyle(
                                              color: Color(0xFF32A2FA),
                                              fontWeight: FontWeight.w600,
                                              fontSize: 16),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          ClipRRect(
                            borderRadius:
                                        BorderRadius.all(Radius.circular(30.0)),
                            child: BackdropFilter(
                              filter: new ImageFilter.blur(
                                  sigmaX: 20.0, sigmaY: 20.0),
                              child: new Container(
                                width: MediaQuery.of(context).size.width * 0.415,
                                height: 180.0,
                                decoration: new BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(30.0)),
                                    color: Color(0xFF4b4c6d).withOpacity(0.5)),
                                child: new Center(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 36.0),
                                    child: Column(
                                      children: <Widget>[
                                        CircleButton(
                                          onTap: () => print(""),
                                          iconData: Icons.directions_bus,
                                          colorR: Color(0xFF7F55FF),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 14.0),
                                        ),
                                        Text(
                                          'Transport',
                                          style: TextStyle(
                                              color: Color(0xFF7F55FF),
                                              fontWeight: FontWeight.w600,
                                              fontSize: 16),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 28.0, right: 28.0, top: 8.0, bottom: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          ClipRRect(
                            borderRadius:
                                        BorderRadius.all(Radius.circular(30.0)),
                            child: BackdropFilter(
                              filter: new ImageFilter.blur(
                                  sigmaX: 20.0, sigmaY: 20.0),
                              child: new Container(
                                width: MediaQuery.of(context).size.width * 0.415,
                                height: 180.0,
                                decoration: new BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(30.0)),
                                    color: Color(0xFF4b4c6d).withOpacity(0.5)),
                                child: new Center(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 36.0),
                                    child: Column(
                                      children: <Widget>[
                                        CircleButton(
                                          onTap: () => print(""),
                                          iconData: Icons.shopping_basket,
                                          colorR: Color(0xFFFF48E1),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 14.0),
                                        ),
                                        Text(
                                          'Shopping',
                                          style: TextStyle(
                                              color: Color(0xFFFF48E1),
                                              fontWeight: FontWeight.w600,
                                              fontSize: 16),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          ClipRRect(
                            borderRadius:
                                        BorderRadius.all(Radius.circular(30.0)),
                            child: BackdropFilter(
                              filter: new ImageFilter.blur(
                                  sigmaX: 20.0, sigmaY: 20.0),
                              child: new Container(
                                width: MediaQuery.of(context).size.width * 0.415,
                                height: 180.0,
                                decoration: new BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(30.0)),
                                    color: Color(0xFF4b4c6d).withOpacity(0.5)),
                                child: new Center(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 36.0),
                                    child: Column(
                                      children: <Widget>[
                                        CircleButton(
                                          onTap: () => print(""),
                                          iconData: Icons.receipt,
                                          colorR: Color(0xFFFF8D48),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 14.0),
                                        ),
                                        Text(
                                          'Bills',
                                          style: TextStyle(
                                              color: Color(0xFFFF8D48),
                                              fontWeight: FontWeight.w600,
                                              fontSize: 16),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 28.0, right: 28.0, top: 8.0, bottom: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          ClipRRect(
                            borderRadius:
                                        BorderRadius.all(Radius.circular(30.0)),
                            child: BackdropFilter(
                              filter: new ImageFilter.blur(
                                  sigmaX: 20.0, sigmaY: 20.0),
                              child: new Container(
                                width: MediaQuery.of(context).size.width * 0.415,
                                height: 180.0,
                                decoration: new BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(30.0)),
                                    color: Color(0xFF4b4c6d).withOpacity(0.5)),
                                child: new Center(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 36.0),
                                    child: Column(
                                      children: <Widget>[
                                        CircleButton(
                                          onTap: () => print(""),
                                          iconData: Icons.local_movies,
                                          colorR: Color(0xFF4D77F2),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 14.0),
                                        ),
                                        Text(
                                          'Movies',
                                          style: TextStyle(
                                              color: Color(0xFF4D77F2),
                                              fontWeight: FontWeight.w600,
                                              fontSize: 16),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          ClipRRect(
                            borderRadius:
                                        BorderRadius.all(Radius.circular(30.0)),
                            child: BackdropFilter(
                              filter: new ImageFilter.blur(
                                  sigmaX: 20.0, sigmaY: 20.0),
                              child: new Container(
                                width: MediaQuery.of(context).size.width * 0.415,
                                height: 180.0,
                                decoration: new BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(30.0)),
                                    color: Color(0xFF4b4c6d).withOpacity(0.5)),
                                child: new Center(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 36.0),
                                    child: Column(
                                      children: <Widget>[
                                        CircleButton(
                                          onTap: () => print(""),
                                          iconData: Icons.fastfood,
                                          colorR: Color(0xFF2EDA63),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 14.0),
                                        ),
                                        Text(
                                          'Grocery',
                                          style: TextStyle(
                                              color: Color(0xFF2EDA63),
                                              fontWeight: FontWeight.w600,
                                              fontSize: 16),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 28.0, right: 28.0, top: 8.0, bottom: 16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          ClipRRect(
                            borderRadius:
                                        BorderRadius.all(Radius.circular(30.0)),
                            child: BackdropFilter(
                              filter: new ImageFilter.blur(
                                  sigmaX: 20.0, sigmaY: 20.0),
                              child: new Container(
                                width: MediaQuery.of(context).size.width * 0.415,
                                height: 180.0,
                                decoration: new BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(30.0)),
                                    color: Color(0xFF4b4c6d).withOpacity(0.5)),
                                child: new Center(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 36.0),
                                    child: Column(
                                      children: <Widget>[
                                        CircleButton(
                                          onTap: () => print(""),
                                          iconData: Icons.filter_vintage,
                                          colorR: Color(0xFFf21a5b),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 14.0),
                                        ),
                                        Text(
                                          'Culture',
                                          style: TextStyle(
                                              color: Color(0xFFf21a5b),
                                              fontWeight: FontWeight.w600,
                                              fontSize: 16),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          ClipRRect(
                            borderRadius:
                                        BorderRadius.all(Radius.circular(30.0)),
                            child: BackdropFilter(
                              filter: new ImageFilter.blur(
                                  sigmaX: 20.0, sigmaY: 20.0),
                              child: new Container(
                                width: MediaQuery.of(context).size.width * 0.415,
                                height: 180.0,
                                decoration: new BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(30.0)),
                                    color: Color(0xFF4b4c6d).withOpacity(0.5)),
                                child: new Center(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 36.0),
                                    child: Column(
                                      children: <Widget>[
                                        CircleButton(
                                          onTap: () => print(""),
                                          iconData: Icons.more_horiz,
                                          colorR: Color(0xFF32A2FA),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 14.0),
                                        ),
                                        Text(
                                          'More',
                                          style: TextStyle(
                                              color: Color(0xFF32A2FA),
                                              fontWeight: FontWeight.w600,
                                              fontSize: 16),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ))),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(
                Icons.calendar_view_day,
                size: 28.0,
                //color: Color(0xFF757594),
              ),
              title: Text('',
                  style: TextStyle(fontWeight: FontWeight.bold, height: 0.0))),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.store_mall_directory,
                size: 28.0,
                //color: Color(0xFF757594),
              ),
              title: Text('',
                  style: TextStyle(fontWeight: FontWeight.bold, height: 0.0))),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.account_circle,
                size: 28.0,
                //color: Color(0xFF757594),
              ),
              title: Text('',
                  style: TextStyle(fontWeight: FontWeight.bold, height: 0.0))),
        ],
        currentIndex: _selectedIndex,
        backgroundColor: Color(0xFF373756),
        selectedItemColor: Color(0xFFF775B0),
        unselectedItemColor: Color(0xFF757594),
        onTap: _onItemTapped,
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}

class CircleButton extends StatelessWidget {
  final GestureTapCallback onTap;
  final IconData iconData;
  final Color colorR;

  const CircleButton({Key key, this.onTap, this.iconData, this.colorR})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double size = 60.0;

    return new InkResponse(
      onTap: onTap,
      child: new Container(
        width: size,
        height: size,
        decoration: new BoxDecoration(
          color: colorR,
          shape: BoxShape.circle,
        ),
        child: new Icon(
          iconData,
          color: Colors.white,
        ),
      ),
    );
  }
}
